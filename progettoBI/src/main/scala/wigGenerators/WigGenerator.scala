package wigGenerators

import main.{FileWIG, SAMLine}

trait WigGenerator {
  /**
    * Populates the feature track array adding information accordingly to the new line of sam file provided.
    * You have to call this method passing all lines of sam file in order to achieve a meaningfull track.
    * @param line The SAMLine to fed.
    */
  def <<(line: SAMLine): Unit

  /**
    * Generate and output the wig file.
    */
  def generateFileLine(): Unit

  def printFile(data: Array[Int], outFile: FileWIG, message: String): Unit = {
    var current = 0
    for(i <- data){
      current += i
      outFile.printFixedLine(current)
    }
    println(message)
  }
}
