package wigGenerators

import main.{FileWIG, SAMLine}

case class Hs(len: Int, outPath: String) extends WigGenerator {
  val data = new Array[Int](len)
  val outFile: FileWIG = new FileWIG(outPath)

  override def <<(line: SAMLine): Unit = {
    if(line.hasMultipleSegments && line.templateLength > 0 && line.cigar.contains("H")){
      data(line.position) += 1
      data(line.matePosition + line.sequence.length) -= 1
    }
  }

  override def generateFileLine(): Unit = {
    outFile.printTrackHead(name = "H", color = "150,0,150")
    outFile.printTrack(true,"genome",1,1,1)

    printFile(data, outFile, "H done")
  }

}
