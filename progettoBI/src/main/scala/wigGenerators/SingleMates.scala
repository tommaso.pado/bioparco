package wigGenerators

import main.{FileWIG, SAMLine}

case class SingleMates(len: Int, outPath: String) extends WigGenerator {
  val data = new Array[Int](len)
  val outFile: FileWIG = new FileWIG(outPath)

  override def <<(line: SAMLine): Unit = {
    if(line.hasMultipleSegments && line.nextSegmentUnmapped && !line.segmentUnmapped) {
      data(line.position) += 1
      data(line.matePosition + line.sequence.length) -= 1
    }
  }

  override def generateFileLine(): Unit = {
    outFile.printTrackHead(name = "Single_Mates", color = "200,200,200")
    outFile.printTrack(true,"genome",1,1,1)

    printFile(data, outFile, "single mates done")
  }

}
