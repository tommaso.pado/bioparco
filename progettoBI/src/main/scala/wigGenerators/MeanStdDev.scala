package wigGenerators
import main.{FileSAM, FileWIG, SAMLine}
import scala.math._

import scala.collection.mutable

case class MeanStdDev(fileIn: FileSAM, outPath: String) extends WigGenerator {
  val thresholdUP = 10000
  fileIn.parseHeader()
  val data = new Array[Int](fileIn.getReferenceLength)
  val outFile: FileWIG = new FileWIG(outPath)
  val genomicInsert = new mutable.Queue[Double]()

  def computeMeanAndSTD(): Array[Double] = {
    var line = fileIn.nextLine()
    println("Computing mean and STD...")

    while (line != null) {
      if (line.hasMultipleSegments && !line.segmentUnmapped && line.templateLength > 0 &&
            line.templateLength < thresholdUP && !line.nextSegmentUnmapped && line.eachSegmentProperlyAligned) {
        genomicInsert += line.templateLength
      }
      line = fileIn.nextLine()
    }


//    println(genomicInsert.map(x => x / genomicInsert.size))
    val mean: Double = genomicInsert.sum / genomicInsert.size
//    val aaa = genomicInsert.map(x => pow(x-mean, 2))
//    val std: Double = aaa.sum / genomicInsert.size
    var std:Double = 0
    for (tette <- genomicInsert) {
      std += pow(tette-mean, 2)
    }
    std/=genomicInsert.size

    Array(mean, sqrt(std))
  }

  val meanAndStd: Array[Double] = computeMeanAndSTD()
  val mean = meanAndStd(0)
  val std = meanAndStd(1)
  println(s"mean = $mean")
  println(s"std = $std")

  override def <<(line: SAMLine): Unit = <<(line, 2)

  def <<(line: SAMLine, n: Int): Unit = {
//    println(s"${abs(line.templateLength - mean)} >? ${n*std} ")
    if(line.hasMultipleSegments && !line.segmentUnmapped && line.templateLength > 0  && !line.nextSegmentUnmapped &&
        abs(line.templateLength - mean) > n*std && line.eachSegmentProperlyAligned ){
//      println("culo")
      data(line.position) += 1
      data(line.matePosition + line.sequence.length) -= 1
    }
  }

  override def generateFileLine(): Unit = {
    outFile.printTrackHead(name = "Coverage++", color = "0,0,0")
    outFile.printTrack(true,"genome",1,1,1)

    printFile(data, outFile, "coverage++ done")
  }

}
