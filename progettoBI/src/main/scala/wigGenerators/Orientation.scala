package wigGenerators

import main.{FileWIG, SAMLine}

case class Orientation(len: Int, outPath: String) extends WigGenerator {
  val data = new Array[Int](len)
  val outFile: FileWIG = new FileWIG(outPath)

  override def <<(line: SAMLine): Unit = {
    if(line.hasMultipleSegments && line.eachSegmentProperlyAligned && line.templateLength > 0 && (line.isReverseComplemented != line.isNextSequenceReverseComplemented)){
      data(line.position) += 1
      data(line.matePosition + line.sequence.length) -= 1
    }
  }

  override def generateFileLine(): Unit = {
    outFile.printTrackHead(name = "Orientation", color = "0,0,200")
    outFile.printTrack(true,"genome",1,1,1)

    printFile(data, outFile, "orientation done")
  }
}
