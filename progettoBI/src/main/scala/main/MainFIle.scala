package main

import wigGenerators._

import scala.collection.mutable

/**
  * Created by paolo on 25/08/18.
  */

object HelloWorld {
  //[sam, wig]
  var fileNames: Array[String] = Array[String]()
  var opt: String = ""


  def main(args: Array[String]): Unit = {

    //parse command line args
    if (args.length == 0) {
      println("No input file provided")
      System.exit(0)
    }
    if (args(0)(0) == '-') {
      opt = args(0)
      if (opt == "-h" || opt == "--help") {
        println("./wigtools.sh [option] <file_in.sam> [file_out]")
        println()
        println("option")
        println("\tIf option is left blank if will behave as with -a or --all")
        println("\t-h\t--help :\tshow this help page")
        println("\t-a\t--all :\t\toutput all wig files computed concurrently")
        println("\t--all-non-concurrent :\toutput all wig files computed linearly")
        println("\t-c\t--coverage :\t output only coverage wig file")
        println("\t-u\t--uniques :\t output only uniques wig file")
        println("\t-m\t--multiples :\t output only multiples wig file")
        println("\t-o\t--orientation :\t output only orientation wig file")
        println("\t-s\t--singleMates :\t output only singleMates wig file")
        println("\t-h\t--hard-clipped :\t\t output only hard clipped wig file")
        println("\t-n\t--n-std :\t output only a track with insertion length above or" +
          "below the mean by 2 times the standard deviation")
        println()
        println("<file_in.sam>")
        println("\tsam file or file path to parse")
        println()
        println("file_out")
        println("\tfilename or path (without extension) for the output file(s)")
        System.exit(0)
      }
      fileNames = readFileNames(args drop 1)
    } else {
      fileNames = readFileNames(args)
    }


    val fileIn = new FileSAM(fileNames(0))




    val outFileNames = getOutFileNames(opt)



    var parsedLength : Long = 0

    parsedLength += fileIn.parseHeader().length()



    var coverage: Coverage = null
    var uniques: Uniques = null
    var multiples: Multiples = null
    var orientation: Orientation = null
    var singleMates: SingleMates = null
    var hs: Hs = null
    var meanStdDev: MeanStdDev = null

    //creating objects
    opt match {
      case "" | "-a" | "--all" | "--all-non-concurrent" =>
        coverage = Coverage(fileIn.getReferenceLength, outFileNames("coverage"))
        uniques = Uniques(fileIn.getReferenceLength, outFileNames("uniques"))
        multiples = Multiples(fileIn.getReferenceLength, outFileNames("multiples"))
        orientation = Orientation(fileIn.getReferenceLength, outFileNames("orientation"))
        singleMates = SingleMates(fileIn.getReferenceLength, outFileNames("singleMates"))
        hs = Hs(fileIn.getReferenceLength, outFileNames("hs"))
        meanStdDev = MeanStdDev(new FileSAM(fileNames(0)), outFileNames("meanStdDev"))
      case "-c" | "--coverage" => coverage = Coverage(fileIn.getReferenceLength, outFileNames("coverage"))
      case "-u" | "--uniques" => uniques = Uniques(fileIn.getReferenceLength, outFileNames("uniques"))
      case "-m" | "--multiples" => multiples = Multiples(fileIn.getReferenceLength, outFileNames("multiples"))
      case "-o" | "--orientation" => orientation = Orientation(fileIn.getReferenceLength, outFileNames("orientation"))
      case "-s" | "--singleMates" => singleMates = SingleMates(fileIn.getReferenceLength, outFileNames("singleMates"))
      case "-h" | "--hard-clipped" => hs = Hs(fileIn.getReferenceLength, outFileNames("hs"))
      case "-n" | "--n-std" => meanStdDev = MeanStdDev(new FileSAM(fileNames(0)), outFileNames("meanStdDev"))
      case _ => System.exit(0)
    }
//    val coverage = Coverage(fileIn.getReferenceLength, outFileNames("coverage"))
//    val uniques = Uniques(fileIn.getReferenceLength, outFileNames("uniques"))
//    val multiples = Multiples(fileIn.getReferenceLength, outFileNames("multiples"))
//    val orientation = Orientation(fileIn.getReferenceLength, outFileNames("orientation"))
//    val singleMates = SingleMates(fileIn.getReferenceLength, outFileNames("singleMates"))
//    val hs = Hs(fileIn.getReferenceLength, outFileNames("hs"))




    var line = fileIn.nextLine()
    println("Parsing input file")
    var lastPrint : Double = 0

    while(line != null){
      parsedLength += line.byteLengthOfLine
      val completion : Double = parsedLength*100 / fileIn.length()
      if(completion - lastPrint >= 2) {
        lastPrint = completion
        print("\r Completed at: " + f"$completion%1.0f" + " %")
      }

      opt match {
        case "" | "-a" | "--all" | "--all-non-concurrent" =>
          coverage << line
          uniques << line
          multiples << line
          orientation << line
          singleMates << line
          hs << line
          meanStdDev.<<(line,2)
        case "-c" | "--coverage" => coverage << line
        case "-u" | "--uniques" => uniques << line
        case "-m" | "--multiples" => multiples << line
        case "-o" | "--orientation" => orientation << line
        case "-s" | "--singleMates" => singleMates << line
        case "-h" | "--hard-clipped" => hs << line
        case "-n" | "--n-std" => meanStdDev << line
        case _ => System.exit(0)
      }

      line = fileIn.nextLine()
    }
    println("\r Completed at: 100 %")
    println("Writing output files...")



    opt match {
      case "" | "-a" | "--all" =>
        new Thread(() => coverage.generateFileLine()).start()
        new Thread(() => uniques.generateFileLine()).start()
        new Thread(() => multiples.generateFileLine()).start()
        new Thread(() => orientation.generateFileLine()).start()
        new Thread(() => singleMates.generateFileLine()).start()
        new Thread(() => hs.generateFileLine()).start()
        new Thread(() => meanStdDev.generateFileLine()).start()
      case "--all-non-concurrent" =>
        coverage.generateFileLine()
        uniques.generateFileLine()
        multiples.generateFileLine()
        orientation.generateFileLine()
        singleMates.generateFileLine()
        hs.generateFileLine()
      case "-c" | "--coverage" => coverage.generateFileLine()
      case "-u" | "--uniques" => uniques.generateFileLine()
      case "-m" | "--multiples" => multiples.generateFileLine()
      case "-o" | "--orientation" => orientation.generateFileLine()
      case "-s" | "--singleMates" => singleMates.generateFileLine()
      case "-h" | "--hard-clipped" => hs.generateFileLine()
      case "-n" | "--n-std" => meanStdDev.generateFileLine()
      case _ => System.exit(0)
    }

  }



  def readFileNames(files: Array[String]): Array[String] = {
    if (files.length == 0) {
      println("No input file provided")
      System.exit(0)
    } else if (files.length == 1) {
      return Array(files(0), "output.wig")
    } else if (files.length == 2) {
      return Array(files(0), files(1))
    } else {
      println("Too many arguments")
      System.exit(0)
    }
    Array("", "")
  }

  def getOutFileNames(opt: String) : mutable.HashMap[String, String] = {
    opt match {
      case "" | "-a" | "--all" | "--all-non-concurrent" => mutable.HashMap(
          ("coverage", fileNames(1) + "_coverage.wig"),
          ("uniques", fileNames(1) + "_uniques.wig"),
          ("multiples", fileNames(1) + "_multiples.wig"),
          ("orientation", fileNames(1) + "_orientation.wig"),
          ("singleMates", fileNames(1) + "_singleMates.wig"),
          ("hs", fileNames(1) + "_hs.wig"),
          ("meanStdDev", fileNames(1) + "_meanStdDev.wig")
        )
      case "-c" | "--coverage" => mutable.HashMap(("coverage", fileNames(1) + "_coverage.wig"))
      case "-u" | "--uniques" => mutable.HashMap(("uniques", fileNames(1) + "_uniques.wig"))
      case "-m" | "--multiples" => mutable.HashMap(("multiples", fileNames(1) + "_multiples.wig"))
      case "-o" | "--orientation" => mutable.HashMap(("orientation", fileNames(1) + "_orientation.wig"))
      case "-s" | "--singleMates" => mutable.HashMap(("singleMates", fileNames(1) + "_singleMates.wig"))
      case "-h" | "--hard-clipped" => mutable.HashMap(("hs", fileNames(1) + "_hs.wig"))
      case "-n" | "--n-std" => mutable.HashMap(("meanStdDev", fileNames(1) + "_meanStdDev.wig"))
      case _ =>
        println("wrong option see -h or --help")
        System.exit(0)
        mutable.HashMap()
    }
  }

}