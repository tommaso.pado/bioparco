package main
import java.io.{BufferedReader, File, FileReader}

import scala.collection.mutable.HashMap
import scala.util.matching.Regex

/**
  * Created by paolo on 25/08/18.
  *
  * Class that handles parsing of a .sam file
  */
case class FileSAM(path : String) extends File(path){
  /**
    * header saved as a Map that has the tag as key
    * e.g. SN:genome has "SN" as key, "genome" as value
    */
  val header = new HashMap[String, String]()
  /**
    * Reader used to parse the lines (NOT HEADER)
    */
  val lineReader = new BufferedReader(new FileReader(this))

  /**
    * Function that parse the header
    * @return the header as a string
    */
  def parseHeader(): String = {
    // string used to return header
    var headerStr = ""
    // new reader to parse the header cause the other is used to parse lines in order
    val reader = new BufferedReader(new FileReader(this))
    // input one line
    var line = reader.readLine()
    // if it is a header line
    while(line.matches(FileSAM.headerLineRegexString)) {
      // append string to headerStr
      headerStr += line
      // find all the different tags in this line
      val fields = FileSAM.headerFieldRegex.findAllIn(line)
      // for each tag
      while (fields.hasNext) {
        // separate the tag and it's value
        val field = fields.next.split(":")
        // put information in the header
        header.put(field(0), field(1))
      }
      // read another line
      line = reader.readLine()
    }
    // close the reader when finish
    reader.close()
    // return the header as a string
    headerStr
  }

  /**
    * function that returns next alignment as a SAMLine
    * using the instance's reader
    *
    * @return a SAMLine representing the alignment, null if EOF
    */
  def nextLine(): SAMLine = {
    // read a line
    val line = lineReader.readLine()
    // if the line is null the EOF is reached, so return null
    if(line == null)
      return null
    // if this line is a header line call recursively this function
    if(line.matches(FileSAM.headerLineRegexString))
      nextLine()
    // if this line is a good line
    else {
      // build and return a SAMLine
      new SAMLine(line)
    }
  }

  /**
    * function that searches in header if there is the tag for sequence length
    *
    * @return the sequence length if there is, 0 otherwise
    */
  def getReferenceLength: Int ={
    // search if there is the tag for sequence length
    if(header.contains(FileSAM.SEQUENCE_LENGTH_TAG)){
      // return the number if found
      return header(FileSAM.SEQUENCE_LENGTH_TAG).toInt
    }
    // return 0 if there is no length
    0
  }

  def flush: Unit = {
    lineReader.reset()
  }

}

/**
  * Companion object of FileSam
  */
object FileSAM{
  /**
    * regex (String) for a header line
    */
  val headerLineRegexString = "@.*"
  /**
    * regex (Regex) for a header TAG:value
    */
  val headerFieldRegex : Regex = "[A-Z]{2}:[^\\s]*".r
  /**
    * Tag for reference sequence length
    */
  val SEQUENCE_LENGTH_TAG = "LN"
}