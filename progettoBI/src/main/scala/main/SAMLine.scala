package main
/**
  * Class that represents an alignment of a .sam file
  *
  * @param array of strings that stores the fields of the line
  * @param length length in bytes of the line
  */
case class SAMLine(array : Array[String], length : Int){
  /**
    * fields of the line
    */
  val ID : String = array(SAMLine.ID_INDEX)
  val flag : Int = array(SAMLine.FLAG_INDEX).toInt
  val referenceName : String = array(SAMLine.REFERENCE_NAME_INDEX)
  val position : Int = array(SAMLine.POSITION_INDEX).toInt
  val mappingQuality : Int = array(SAMLine.MAPPING_QUALITY_INDEX).toInt
  val cigar : String = array(SAMLine.CIGAR_INDEX)
  val mateID : String = array(SAMLine.MATE_ID_INDEX)
  val matePosition : Int = array(SAMLine.MATE_POSITION_INDEX).toInt
  val templateLength : Int = array(SAMLine.TEMPLATE_LENGTH_INDEX).toInt
  val sequence : String = array(SAMLine.SEQUENCE_INDEX)
  val sequenceQuality : String = array(SAMLine.SEQUENCE_QUALITY_INDEX)

  /**
    * length in bytes of the line (used mostly to keep track of position in the file)
    */
  var byteLengthOfLine : Int = length

  /**
    * secondary constructor from a string (suggested choice)
    *
    * @param line the line as astrign
    */
  def this(line : String){
    // call the primary constructor splitting string on spaces and with it's length
    this(line.split("\\s"), line.length)
  }

  def hasMultipleSegments: Boolean ={
    (flag & 1) == 1
  }

  def eachSegmentProperlyAligned: Boolean ={
    (flag & 2) == 2
  }

  def segmentUnmapped: Boolean ={
    (flag & 4) == 4
  }

  def nextSegmentUnmapped: Boolean ={
    (flag & 8) == 8
  }

  def isReverseComplemented: Boolean ={
    (flag & 16) == 16
  }

  def isNextSequenceReverseComplemented: Boolean ={
    (flag & 32) == 32
  }

  def isFirstSegment: Boolean  ={
    (flag & 64) == 64
  }

  def isLastSegment: Boolean ={
    (flag & 128) == 128
  }

  def hasSecondaryAlignment: Boolean ={
    (flag & 256) == 256
  }

  def notPassingFilters: Boolean ={
    (flag & 512) == 512
  }

  def PCRorOpticalDuplicate: Boolean ={
    (flag & 1024) == 1024
  }

  def supplementaryAlignment: Boolean ={
    (flag & 2048) == 2048
  }

}

/**
  * Companion object of SAMLine
  */
object SAMLine{
  /**
    * Indexes of the fields in the array
    */
  val ID_INDEX = 0
  val FLAG_INDEX = 1
  val REFERENCE_NAME_INDEX = 2
  val POSITION_INDEX = 3
  val MAPPING_QUALITY_INDEX = 4
  val CIGAR_INDEX = 5
  val MATE_ID_INDEX = 6
  val MATE_POSITION_INDEX = 7
  val TEMPLATE_LENGTH_INDEX = 8
  val SEQUENCE_INDEX = 9
  val SEQUENCE_QUALITY_INDEX = 10
}