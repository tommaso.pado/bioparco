package main
import java.io.{BufferedWriter, File, FileWriter}

/**
  * Created by paolo on 27/08/18.
  */
class FileWIG (path : String) extends File(path){
  val writer = new BufferedWriter(new FileWriter(this))


  def printTrackHead(name : String = "User Track", description : String = "User Supplied Track", visibility :String = FileWIG.VISIBILITY_HIDE, color : String = "255,255,255", priority : Int = 100): Unit ={
    writer.write("track type=wiggle_0 ")
    writer.write("name=\""+name+"\" ")
    writer.write("description=\""+description+"\" ")
    writer.write(s"visibility=$visibility ")
    writer.write(s"color=$color ")
    writer.write(s"priority=$priority ")
    writer.write("\n")

  }
  def printTrack(fixed : Boolean, chrom: String ="chr1", start : Int, step : Int = 1, span : Int = 1): Unit ={
    if(fixed)
      writer.write("fixedStep ")
    else
      writer.write("variableStep ")

    writer.write(s"chrom=$chrom ")

    if(fixed){
      writer.write(s"start=$start ")
      writer.write(s"step=$step ")
    }
    writer.write(s"span=$span ")

    writer.write("\n")
  }

  def printFixedLine(value : Double): Unit ={
    writer.write(f"$value%1.2f" + "\n")
  }

  def printVariableLine(position : Int, value: Double): Unit ={
    writer.write(position + " " + f"$value%1.2f" + "\n")
  }




}

object FileWIG{
  val VISIBILITY_FULL = "full"
  val VISIBILITY_HIDE = "hide"
  val VISIBILITY_DENSE = "dense"
}
